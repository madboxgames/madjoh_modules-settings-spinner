define([
	'require',
	'displayers/overlays/spinner'
], 
function (require, SpinnerPage){
	var Spinner = {
		page : null,

		init : function(){
			if(Spinner.page) return;
			Spinner.page = new SpinnerPage();
			Spinner.page.build(Spinner);
		},

		setVisible : function(visible){
			if(visible) Spinner.page.show();
			else Spinner.page.close();
		},
		onDisappeared : function(){
			Spinner.page.hide();
		}
	};

	return Spinner;
});

